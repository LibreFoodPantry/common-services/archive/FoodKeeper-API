package com.gitlab.LibreFoodPantry.FoodKeeperAPI.Models;

import java.util.Map;

import com.gitlab.LibreFoodPantry.FoodKeeperAPI.JsonConverter;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Class representing a cooking method tracked by the application")
public class CookingTip {
	@ApiModelProperty(notes = "Unique identifier for the cooking tip", example = "1", required = true, position = 0)
	private Integer id;
	@ApiModelProperty(notes = "Reference to product with specified id", example = "2", required = true, position = 1)
	private Integer productId;
	@ApiModelProperty(notes = "Tips for cooking", example = "Cook until yolk and white are firm", required = true, position = 2)
	private String tips;
	@ApiModelProperty(notes = "Safe minimum temperature", example = "145", position = 3)
	private Integer safeMinTemp;
	@ApiModelProperty(notes = "Rest time", example = "5", position = 4)
	private Integer restTime;
	@ApiModelProperty(notes = "Metric for rest time", example = "minutes", position = 5)
	private String restTimeMetric;
	
	public CookingTip() {}
	
	public CookingTip(Integer id, Integer productID, String tips, Integer safeMinTemp, Integer restTime,
			String restTimeMetric) {
		this.id = id;
		this.productId = productID;
		this.tips = tips;
		this.safeMinTemp = safeMinTemp;
		this.restTime = restTime;
		this.restTimeMetric = restTimeMetric;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public Integer getProductID() {
		return productId;
	}

	public void setProductID(Integer productID) {
		this.productId = productID;
	}

	public String getTips() {
		return tips;
	}

	public void setTips(String tips) {
		this.tips = tips;
	}

	public Integer getSafeMinTemp() {
		return safeMinTemp;
	}

	public void setSafeMinTemp(Integer safeMinTemp) {
		this.safeMinTemp = safeMinTemp;
	}

	public Integer getRestTime() {
		return restTime;
	}

	public void setRestTime(Integer restTime) {
		this.restTime = restTime;
	}

	public String getRestTimeMetric() {
		return restTimeMetric;
	}

	public void setRestTimeMetric(String restTimeMetric) {
		this.restTimeMetric = restTimeMetric;
	}

	@Override
	public String toString() {
		return "CookingTip [id=" + id + ", productID=" + productId + ", tips=" + tips + ", safeMinTemp=" + safeMinTemp
				+ ", restTime=" + restTime + ", restTimeMetric=" + restTimeMetric + "]";
	}
	
	public static CookingTip getFromJsonArray(JsonArray jsonArr) {
		Map<String, JsonElement> map = JsonConverter.getMapFromJsonArray(jsonArr);
		CookingTip newCookingTip = new CookingTip();
		
		newCookingTip.id = JsonConverter.getJsonElementAsInt(map.get("ID"));
		newCookingTip.productId = JsonConverter.getJsonElementAsInt(map.get("Product_ID"));
		newCookingTip.tips = JsonConverter.getJsonElementAsString(map.get("Tips"));
		newCookingTip.safeMinTemp = JsonConverter.getJsonElementAsInt(map.get("Safe_Minimum_Temperature"));
		newCookingTip.restTime = JsonConverter.getJsonElementAsInt(map.get("Rest_Time"));
		newCookingTip.restTimeMetric = JsonConverter.getJsonElementAsString(map.get("Rest_Time_metric"));
		
		return newCookingTip;
	}
}
