package com.gitlab.LibreFoodPantry.FoodKeeperAPI.Controllers;

import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import io.swagger.annotations.ApiOperation;

@RestController
public class BaseController {		
	@GetMapping(value = "/")
	@ApiOperation("Redirects base url '/' to swagger-ui.html")
	public ModelAndView redirectToSwaggerUI(ModelMap model) {
		return new ModelAndView("redirect:/swagger-ui.html", model);
	}
}