package com.gitlab.LibreFoodPantry.FoodKeeperAPI.Controllers;

import java.util.Collection;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gitlab.LibreFoodPantry.FoodKeeperAPI.Database;
import com.gitlab.LibreFoodPantry.FoodKeeperAPI.Models.CookingTip;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/cookingTips")
@Api()
public class CookingTipController {
	private Map<Integer, CookingTip> cookingTipDb = Database.getCookingTipDb();
	
	@GetMapping(value = "")
	@ApiOperation("Returns a list of all cooking tips")
	public ResponseEntity<Collection<CookingTip>> getAllCookingTips() {
		if (cookingTipDb != null) {
			return new ResponseEntity<>(cookingTipDb.values(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
	}
	
	@GetMapping(value = "/{id}")
	@ApiOperation("Returns a specific cooking tip based on its unique identifier")
	public ResponseEntity<CookingTip> getCookingTipById(@PathVariable("id") Integer id) {
		if (cookingTipDb.containsKey(id)) {
			return new ResponseEntity<>(cookingTipDb.get(id), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
	}
}
