package com.gitlab.LibreFoodPantry.FoodKeeperAPI.Controllers;

import java.util.Collection;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gitlab.LibreFoodPantry.FoodKeeperAPI.Database;
import com.gitlab.LibreFoodPantry.FoodKeeperAPI.Models.Category;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/categories")
@Api()
public class CategoryController {
	private static Map<Integer, Category> categoryDb = Database.getCategoryDb();
	
	@GetMapping(value = "")
	@ApiOperation("Returns a list of all Categories in the system")
	public ResponseEntity<Collection<Category>> getAllCategories() {
		if (categoryDb != null) {
			return new ResponseEntity<>(categoryDb.values(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
	}
	
	@GetMapping(value = "/{id}")
	@ApiOperation("Returns a specific category based on its unique identifier")
	public ResponseEntity<Category> getCategoryById(@PathVariable("id") Integer id) {
		if (categoryDb.containsKey(id)) {
			return new ResponseEntity<>(categoryDb.get(id), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
	}
}
