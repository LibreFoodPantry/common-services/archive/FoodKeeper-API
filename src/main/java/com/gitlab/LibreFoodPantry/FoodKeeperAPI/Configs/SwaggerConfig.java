package com.gitlab.LibreFoodPantry.FoodKeeperAPI.Configs;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2)  
		          .select()                                  
		          .apis(RequestHandlerSelectors.basePackage("com.gitlab.LibreFoodPantry.FoodKeeperAPI"))              
		          .paths(PathSelectors.any())                          
		          .build()
		          .apiInfo(apiInfo());
	}
	
	private ApiInfo apiInfo() {
		return new ApiInfoBuilder()
				  .title("FoodKeeper API")
				  .description("API for USDA FSIS FoodKeeper data (https://gitlab.com/LibreFoodPantry/FoodKeeper-API)")
				  .license("GNU General Public License v3.0")
				  .licenseUrl("https://www.gnu.org/licenses/gpl-3.0.en.html")
				  .termsOfServiceUrl("")
				  .version("1.2.0")
				  .contact(new Contact("", "", ""))
				  .build();
	}
}